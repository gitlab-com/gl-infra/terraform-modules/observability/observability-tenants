# Observability Tenants Terraform Module

## What is this?

This is a simple module that provisions an observability tenant credential and stores it in Hashicorp Vault.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.


<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.5 |
| <a name="requirement_grafana"></a> [grafana](#requirement\_grafana) | >= 2.5 |
| <a name="requirement_htpasswd"></a> [htpasswd](#requirement\_htpasswd) | >= 1.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | >= 3.5 |
| <a name="requirement_vault"></a> [vault](#requirement\_vault) | >= 3.23 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_grafana"></a> [grafana](#provider\_grafana) | >= 2.5 |
| <a name="provider_htpasswd"></a> [htpasswd](#provider\_htpasswd) | >= 1.0 |
| <a name="provider_random"></a> [random](#provider\_random) | >= 3.5 |
| <a name="provider_vault"></a> [vault](#provider\_vault) | >= 3.23 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [grafana_data_source.loki](https://registry.terraform.io/providers/grafana/grafana/latest/docs/resources/data_source) | resource |
| [grafana_data_source.mimir](https://registry.terraform.io/providers/grafana/grafana/latest/docs/resources/data_source) | resource |
| [htpasswd_password.hash](https://registry.terraform.io/providers/loafoe/htpasswd/latest/docs/resources/password) | resource |
| [random_password.password](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [random_password.salt](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [vault_kv_secret_v2.tenant_credential](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kv_secret_v2) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_grafana"></a> [grafana](#input\_grafana) | Grafana datasource configuration. | <pre>object({<br>    loki = optional(object({<br>      enabled = optional(bool, true)<br>      url     = optional(string, "")<br>    }), {})<br>    mimir = optional(object({<br>      enabled = optional(bool, true)<br>      url     = optional(string, "")<br>    }), {})<br>  })</pre> | `{}` | no |
| <a name="input_is_admin_user"></a> [is\_admin\_user](#input\_is\_admin\_user) | Creates tenant as an admin user without datasources. | `bool` | `false` | no |
| <a name="input_tenant_credential_settings"></a> [tenant\_credential\_settings](#input\_tenant\_credential\_settings) | Override settings to the random\_password resource generation. | <pre>object({<br>    length           = optional(number, 24)<br>    special          = optional(bool, true)<br>    override_special = optional(string, "")<br>  })</pre> | `{}` | no |
| <a name="input_tenant_name"></a> [tenant\_name](#input\_tenant\_name) | Tenant name. | `string` | n/a | yes |
| <a name="input_uid"></a> [uid](#input\_uid) | Tenant UID. If left unset a random one will be generated. | `string` | `""` | no |
| <a name="input_vault_mount_path"></a> [vault\_mount\_path](#input\_vault\_mount\_path) | Vault secrets backend to mount path. | `string` | n/a | yes |
| <a name="input_vault_secret_path"></a> [vault\_secret\_path](#input\_vault\_secret\_path) | Path to where the tenant secret is stored in vault. This is combined with tenant name for full path. | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->