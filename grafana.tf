# Create a Mimir datasource per tenant
resource "grafana_data_source" "mimir" {
  count = var.is_admin_user || !var.grafana.mimir.enabled ? 0 : 1

  name                = format("Mimir - %s", title(replace(var.tenant_name, "-", " ")))
  uid                 = local.mimir_uid
  type                = "prometheus"
  url                 = var.grafana.mimir.url
  is_default          = false
  basic_auth_enabled  = true
  basic_auth_username = var.tenant_name

  json_data_encoded = jsonencode({
    httpMethod        = "POST"
    prometheusType    = "Mimir"
    prometheusVersion = "2.9.1"
    manageAlerts      = true
  })

  http_headers = {
    "X-Scope-OrgID" = var.tenant_name
  }

  secure_json_data_encoded = jsonencode({
    basicAuthPassword = random_password.password.result
  })
}

# Create a Loki datasource per tenant
resource "grafana_data_source" "loki" {
  count = var.is_admin_user || !var.grafana.loki.enabled ? 0 : 1

  name                = format("Loki - %s", title(replace(var.tenant_name, "-", " ")))
  uid                 = local.loki_uid
  type                = "loki"
  url                 = var.grafana.loki.url
  is_default          = false
  basic_auth_enabled  = true
  basic_auth_username = var.tenant_name

  json_data_encoded = jsonencode({
    httpMethod   = "POST"
    manageAlerts = true
    maxLines     = "5000"
  })

  http_headers = {
    "X-Scope-OrgID" = var.tenant_name
  }

  secure_json_data_encoded = jsonencode({
    basicAuthPassword = random_password.password.result
  })
}
