locals {
  mimir_uid = coalesce(var.uid, format("mimir-%s", var.tenant_name))
  loki_uid  = coalesce(var.uid, format("loki-%s", var.tenant_name))
}
