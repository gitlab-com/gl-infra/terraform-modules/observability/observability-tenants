resource "random_password" "password" {
  length           = var.tenant_credential_settings.length
  special          = var.tenant_credential_settings.special
  override_special = var.tenant_credential_settings.override_special

  # This is set to help import existing credentials.
  lifecycle {
    ignore_changes = [
      length,
      special,
      override_special,
    ]
  }
}

resource "random_password" "salt" {
  length = 30
}

resource "htpasswd_password" "hash" {
  password = random_password.password.result
  salt     = substr(sha512(random_password.salt.result), 0, 8)
}

# Save tenant credentials to vault
resource "vault_kv_secret_v2" "tenant_credential" {
  mount               = var.vault_mount_path
  name                = format("%s/%s", var.vault_secret_path, var.tenant_name)
  delete_all_versions = true
  data_json = jsonencode({
    password        = random_password.password.result
    password_apr1   = htpasswd_password.hash.apr1
    password_bcrypt = htpasswd_password.hash.bcrypt
    username        = var.tenant_name
    }
  )
}
