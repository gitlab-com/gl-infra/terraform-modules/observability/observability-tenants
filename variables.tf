variable "vault_mount_path" {
  type        = string
  description = "Vault secrets backend to mount path."
}

variable "vault_secret_path" {
  type        = string
  description = "Path to where the tenant secret is stored in vault. This is combined with tenant name for full path."
}

variable "is_admin_user" {
  type        = bool
  description = "Creates tenant as an admin user without datasources."
  default     = false
}

variable "tenant_name" {
  type        = string
  description = "Tenant name."
  validation {
    condition     = can(regexall("^[a-z0-9]+(?:-[a-z0-9]+)*$", var.tenant_name))
    error_message = "Tenant names must be in the format `^[a-z0-9]+(?:-[a-z0-9]+)*$`. Lower case alphanumeric with hyphens only."
  }
}

variable "uid" {
  type        = string
  description = "Tenant UID. If left unset a random one will be generated."
  default     = ""
  validation {
    condition     = can(regexall("^[a-z0-9]+(?:-[a-z0-9]+)*$", var.uid))
    error_message = "UIDs must be in the format `^[a-z0-9]+(?:-[a-z0-9]+)*$`. Lower case alphanumeric with hyphens only."
  }
}

variable "tenant_credential_settings" {
  type = object({
    length           = optional(number, 24)
    special          = optional(bool, true)
    override_special = optional(string, "")
  })
  description = "Override settings to the random_password resource generation."
  default     = {}
}

variable "grafana" {
  type = object({
    loki = optional(object({
      enabled = optional(bool, true)
      url     = optional(string, "")
    }), {})
    mimir = optional(object({
      enabled = optional(bool, true)
      url     = optional(string, "")
    }), {})
  })
  description = "Grafana datasource configuration."
  default     = {}

  validation {
    condition     = var.grafana.loki.url != "" && var.grafana.loki.enabled || !var.grafana.loki.enabled
    error_message = "Must set a url for grafana.loki.url when the datasource is enabled."
  }
  validation {
    condition     = var.grafana.mimir.url != "" && var.grafana.mimir.enabled || !var.grafana.mimir.enabled
    error_message = "Must set a url for grafana.mimir.url when the datasource is enabled."
  }
}
