terraform {
  required_version = ">= 1.5"

  required_providers {
    vault = {
      source  = "hashicorp/vault"
      version = ">= 3.23"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 3.5"
    }
    grafana = {
      source  = "grafana/grafana"
      version = ">= 2.5"
    }
    htpasswd = {
      source  = "loafoe/htpasswd"
      version = ">= 1.0"
    }
  }
}
